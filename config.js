/**
 * Project configuration
 * 
 * @author Pierre HUBERT
 */

/**
 * The port into which the server listen
 */
exports.port = 8081;