/**
 * Connections sockets management
 * 
 * @author Pierre HUBERT
 */

let SocketsList = [];

/**
 * Send a message to the socket
 * 
 * @param socket The target socket
 * @param {Object} message The message to send to the socket
 */
function SendMessageToSocket(socket, message){
    socket.send(JSON.stringify(message));
}

/**
 * Add a socket to the list
 * 
 * @param {String} id The ID of the socket
 * @param socket The socket to add
 */
function AddSocketToList(id, socket){

    SocketsList.push({
        id: id,
        socket: socket
    });

}

/**
 * Remove the socket from the list
 * 
 * @param  socket Socket to remove
 */
function RemoveSocketFromList(socket){
    
    for(let i = 0; i < SocketsList.length; i++){
        
        if(SocketsList[i].socket == socket){
            SocketsList.splice(i, 1);
            i--; 
        }

    }

}

/**
 * Send message to a target to inform that source is ready to establish
 * a connection
 * 
 * @param {String} source_id The ID of the source of the message
 * @param {String} target_id The ID of the target of the message
 */
function SendReadyMessageToClient(source_id, target_id){

    let count = 0;

    SocketsList.forEach(info => {

        if(info.id == target_id){
            SendMessageToSocket(info.socket, {
                ready_msg: true,
                source_id: source_id
            });

            count++;
        }

    });

    return count;
}

/**
 * Send a signal to several clients
 * 
 * @param {String} content The content of the signal
 * @param {String} source_id The ID of the source of the message
 * @param {String} target_id The ID of the target client
 * @return {Number} The number of sockets to which the message was sent
 */
function SendSignalToClient(content, source_id, target_id){

    let count = 0;

    SocketsList.forEach(info => {

        if(info.id == target_id){
            SendMessageToSocket(info.socket, {
                signal: content,
                source_id: source_id
            });

            count++;
        }

    });

    return count;

}

/**
 * Add a socket to the list of active sockets
 * 
 * This method is called when the server handles a new connection
 * 
 * @param socket
 */
exports.addSocket = function(socket){

    console.log("New socket connection detected!");

    /**
     * This variable will contains the ID of the client
     * as soon as the client will send it
     */
    let client_id = false;

    /**
     * We listen to socket messages
     */
    socket.on("message",  message => {

        //Make sure the message is not too heavy
        if(message.length > 10000){
            SendMessageToSocket(socket, {
                error: "Your request is too heavy for this server!"
            })
            return;
        }

        //Decode message
        let data;
        try {
            data = JSON.parse(message);
        }
        catch (err) {
            console.error("Invalid message received from socket!");
            SendMessageToSocket(socket, {
                error: "Message could not be parsed!"
            })
            return;
        };

        if(!client_id){

            //Check if the client ID was specified or not in the request
            if(!data.client_id){
                SendMessageToSocket(socket, {
                    error: "Please specify your client ID!"
                });
                return;
            }

            if(typeof data.client_id != "string"){
                SendMessageToSocket(socket, {
                    error: "Invalid client ID specified!"
                });
                return;
            }

            //Save client ID
            client_id = data.client_id;
            AddSocketToList(client_id, socket);
            console.log("New connection from client ID " + client_id);
            SendMessageToSocket(socket, {
                success: "Client ID successfully set."
            });
        }

        //Check if the client wants to send a ready signal to another client
        else if(data.ready_msg && data.target_id){

            if(typeof data.target_id !== "string"){
                SendMessageToSocket(socket, {
                    error: "Unsecure request!"
                });
                return;
            }

            //Send the signal to the target client
            let number_target = SendReadyMessageToClient(client_id, data.target_id);

            SendMessageToSocket(socket, {
                ready_message_sent: true,
                target_id: data.target_id,
                number_of_targets: number_target
            });

        }

        //Check if the client wants to send a signal to another client
        else if(data.signal && data.target_id) {

            if(typeof data.signal !== "string" || typeof data.target_id !== "string"){
                SendMessageToSocket(socket, {
                    error: "Unsecure request!"
                });
                return;
            }

            //Send the signal to the target client
            let number_target = SendSignalToClient(data.signal, client_id, data.target_id);

            SendMessageToSocket(socket, {
                signal_sent: true,
                number_of_targets: number_target
            });
        }

        //Message not understood
        else {
            SendMessageToSocket(socket, {
                error: "Could not understand your request!"
            });
        }
    });
    
    
    socket.on("close", () => {
        if(client_id) {
            console.log("Connection from " + client_id + " closed.");
            RemoveSocketFromList(socket);
        }
    });
}
