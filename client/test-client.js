/**
 * Node Signal Exchanger test client
 * 
 * @author Pierre HUBERT
 */

const Config = {
    port: 8081,
    server_name: "localhost",
    clientID: location.href.toString().includes("#1") ? "client-1" : "client-2",
    otherPeerID: location.href.toString().includes("#1") ? "client-2" : "client-1"
};

let client = new SignalExchangerClient(
    Config.server_name, 
    Config.port, 
    Config.clientID
);

var p;

client.onConnected = function(){
    console.log("Connected to signal exchanger server.");

    InitializePeer();
}

client.onError = function() {
    console.error("An error occurred with the connection to the signal exchanger server");
}

client.onClosed = function() {
    console.log("Connection to server closed.");
}

client.onSignal = function(signal, peerID){

    if(peerID !== Config.otherPeerID)
        return alert("A signal from an unknow peer ID has just been received!");

    p.signal(JSON.parse(signal));


}

client.onReadyMessage = function(peerID) {
    console.log("Peer " + peerID + " sent a signal to inform it is ready to establish a connection...");
}

/**
 * Initialize peer connection
 * 
 * Warning : It is better to wait for the socket connection to be ready before
 * launching this function if this peer is the initiator of the connection
 */
function InitializePeer() {

    p = new SimplePeer({ 
        initiator: location.hash === '#1', 
        trickle: false,
        config: {
            'iceServers': [
            { urls: 'stun:127.0.0.1:3478' },
            {"url":"turn:127.0.0.1:3478", 
                                    "credential":"anonymous",
                                    "username": "anonymous"}
            ]
        }
    });

    p.on('error', function (err) { console.log('SimplePeer error', err) });

    //Automatically send new signals to the other peer
    p.on('signal', function (data) {
        console.log('SIGNAL', JSON.stringify(data))
        client.sendSignal(Config.otherPeerID, JSON.stringify(data));
    });

    p.on('connect', function () {
        console.log('CONNECT');
        msg = 'whatever' + Math.random();
        console.log("Sending: " + msg);
        p.send(msg);
    })

    p.on('data', function (data) {
        console.log('data: ' + data)
    })
}