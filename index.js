/**
 * Project main module
 * 
 * @author Pierre HUBERT
 */

/**
 * Import requirements
 */
const express = require("express");
const app = express();
const WebSocket = require("ws");

/**
 * Import project modules
 */
const Config = require("./config");
const Sockets = require("./sockets");

/**
 * If user tries to connect to home page,
 * show a message to explains him what is the
 * purpose of this service
 */
app.get("/", (req, res) => {
    res.send("Welcome to NodeSignalExchanger!");
});

/**
 * Start express server
 */
var server = app.listen(Config.port, () => {
    console.log("Application listening on port " + Config.port);
});


/**
 * Handles socket connections
 */
var wss = WebSocket.Server({
    server: server,
    path: "/socket"
});

wss.on("connection", socket => {
    Sockets.addSocket(socket);
});
